package be.dasos.learning.okapi;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.common.resource.*;
import net.sf.okapi.filters.xmlstream.Parameters;
import net.sf.okapi.filters.xmlstream.XmlStreamFilter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;

import java.net.URL;

public class Main {
    private static final Logger LOG = LogManager.getFormatterLogger(Main.class);

    public static void main (String... args) throws Exception {
        String file = "document.xml";

        LOG.debug("Creating filter for %s", file);
        IFilter filter = createFilter(file);

        LOG.debug("Start processing filter events (Displayed as EVENT_TYPE[ ID ]");
        while (filter.hasNext()) {
            Event event = filter.next();

            LOG.info("%s[ %s ]\n", event, event.getResource().getId());
            switch (event.getEventType()) {
                case START_DOCUMENT: handleStartDocument(event); break;
                case DOCUMENT_PART:  handleDocumentPart(event); break;
                case TEXT_UNIT:      handleTextUnit(event); break;
                case END_DOCUMENT:   handleEndDocument(event); break;
                default: LOG.info("Unsupported event. :(");
            }
        }
    }

    private static IFilter createFilter(String filename) throws Exception {
        LOG.debug("Creating XmlStream Filter");
        IFilter filter = new XmlStreamFilter();

        LOG.debug("Loading parameters");
        URL parameterFile = Main.class.getResource(Parameters.DITA_PARAMETERS);

        LOG.debug("Fetching custom DITA parameters from %s", parameterFile.getPath());
        IParameters ditaParameters = new Parameters();
        ditaParameters.load(parameterFile.toURI(), false);

        LOG.debug("Applying parameters to filter");
        filter.setParameters(ditaParameters);

        LOG.debug("Preparing resource for reading: " + filename);
        URL resource = Main.class.getResource(filename);
        InputStream in = resource.openStream();

        // --Some filters will be able to automatically detect the correct encoding of the input and ignore this value.
        LOG.debug("Detecting resource encoding");
        String encoding = EncodingDetector.detectEncoding(resource);

        LOG.debug("Setting default locale (en)");
        LocaleId locale = LocaleId.fromString("en");

        LOG.debug("Applying filter to the requested resource");
        RawDocument document = new RawDocument(in, encoding, locale);
        filter.open(document);

        return filter;
    }

    private static void handleDefaultEvent(Event event) {
        //System.out.println(event.getResource());
    }
    private static void handleStartDocument(Event event){
        StartDocument resource = event.getStartDocument();

        handleDefaultEvent(event);
    }

    private static void handleDocumentPart(Event event){
        DocumentPart resource = event.getDocumentPart();

        handleDefaultEvent(event);
    }

    private static void handleTextUnit(Event event){
        ITextUnit resource = event.getTextUnit();
        //System.out.println(resource.getPropertyNames().toString());

        TextFragment tf = resource.getSource().getFirstContent();

        // --The piece of text in href
        System.out.println("\t -->"+tf.getCodedText());
        for(Code c : tf.getCodes())
        {
            System.out.println(
                    "\tid: "  + c.getId()+
                    " - tag: "        + c.getTagType() +
                    ((c.getTagType()== TextFragment.TagType.PLACEHOLDER)?"":"\t" )+
                    " - data: " + c.getData());
        }

    }
    private static void handleEndDocument(Event event){
        Ending resource = event.getEnding();

        handleDefaultEvent(event);
    }
}