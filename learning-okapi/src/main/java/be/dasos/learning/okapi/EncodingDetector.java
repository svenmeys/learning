package be.dasos.learning.okapi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mozilla.universalchardet.UniversalDetector;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public final class EncodingDetector {
    private static final Logger LOG = LogManager.getFormatterLogger(EncodingDetector.class);

    private static final int SAMPLE_SIZE = 4096;
    /**
     * Sample the first few bytes of a resource to determine its encoding
     * @param resource   the URL of a file
     * @throws Exception
     */
    public static String detectEncoding(URL resource) throws IOException {
        UniversalDetector detector = new UniversalDetector(null);
        byte[] buffer = new byte[SAMPLE_SIZE];

        try {
            InputStream in = resource.openStream(); // --Prepare the file for reading

            int nread;
            while ((nread = in.read(buffer)) > 0 && !detector.isDone()) {
                detector.handleData(buffer, 0, nread);  // -- Sample the data
            }
        } catch (IOException e) {
            LOG.error("Unable to read resource [%s] : %s", resource, e.getMessage());
            throw e;
        }
        detector.dataEnd();                     // --Done sampling

        String encoding = detector.getDetectedCharset();
        detector.reset();                       // --Clear the detector for further use

        LOG.debug("Detected encoding = %s", encoding);
        return encoding;
    }
}