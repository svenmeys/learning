package be.dasos.learning.effectivejava;

import com.google.common.base.Preconditions;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

public class Food {
  private final String name;
  private final int weight;
  private final int calories;
  private final int fat;
  private final int carbs;

  public static class Builder {
    // -- Required
    private final String name;
    private final int calories;

    // -- Optional
    private int weight;
    private int fat;
    private int carbs;

    public Builder (String name, int calories) {
      this.name = name;
      this.calories = calories;
    }

    public Builder weight (int weight) {this.weight = weight; return this;}
    public Builder fat    (int fat)    {this.fat    = fat;    return this;}
    public Builder carbs  (int carbs)  {this.carbs  = carbs;  return this;}

    public Food build() throws IllegalStateException {
      checkState(  weight > 0, "Weight must be > 0");
      checkState(     fat > 0, "Fat must be > 0");
      checkState(   carbs > 0, "Carbs must be > 0");
      checkState(calories > 0, "Calories must be > 0");

      return new Food(this);
    }
  }

  private Food(Builder builder) {
    name = builder.name;
    calories = builder.calories;

    weight = builder.weight;
    fat = builder.fat;
    carbs = builder.carbs;
  }

  public static void main(String ... args) {
    Food food = new Builder("Food", 100)
                      .carbs(5)
                      .fat(4)
                      .weight(10)
                      .build();
  }
}
