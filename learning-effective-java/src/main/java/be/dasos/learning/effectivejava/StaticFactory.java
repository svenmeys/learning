package be.dasos.learning.effectivejava;

public class StaticFactory {
  private String param1;
  private String param2;

  private StaticFactory(){}

  public static final StaticFactory createStaticFactory() {
    return new SpecialFactory(); // Can create subclassed objects. Invisible to the user
  }

  private static class SpecialFactory extends StaticFactory{
    private SpecialFactory() {
      super();
    }
  }
}
