package be.dasos.learning.effectivejava;

public class UtilityClass {
  private UtilityClass() { throw new AssertionError(); }

  public static void utilityMethod() {}
}
