package be.dasos.learning.effectivejava;

public enum ElvisSingleton {
  getInstance;

  public void leaveTheBuilding() {
    System.out.println("Leaving the building");
  };

  public static void main(String ... args){
    ElvisSingleton.getInstance.leaveTheBuilding();
  }
}
