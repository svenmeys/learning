package be.dasos.learning.xstream.alias;

import com.thoughtworks.xstream.XStream;

public class Main {
    public static void main(String[] args) {

        Blog teamBlog = new Blog(new Author("Guilherme Silveira"));
        teamBlog.add(new Entry("first","My first blog entry."));
        teamBlog.add(new Entry("tutorial", "Today we have developed a nice alias tutorial. Tell your friends! NOW!"));

        XStream xstream = new XStream();


        // use aliases instead of full class names in xml
        xstream.alias("blog", Blog.class);
        xstream.alias("entry", Entry.class);

        // change the name of the writer field
        xstream.aliasField("author", Blog.class, "writer");

        // Implicit collection: omits the entries tag
        xstream.addImplicitCollection(Blog.class, "entries");

        // Declare attributes
        xstream.useAttributeFor(Blog.class, "writer");
        xstream.registerConverter(new AuthorConvertor());

        System.out.println(xstream.toXML(teamBlog));
    }
}
