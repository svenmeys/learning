package be.dasos.learning.xstream.alias;

public class Author {
    private String name;
    public Author(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
}
