package be.dasos.learning.xstream.twominute;

import com.thoughtworks.xstream.XStream;

public class Main {
    public static void main (String args[]) {
        Main program = new Main();

        String test1Output = program.test1();

        Person newJoe = program.test2(test1Output);
    }

    public String test1() {
        XStream xstream = new XStream();

        xstream.alias("person", Person.class);
        xstream.alias("phone", PhoneNumber.class);

        Person joe = new Person("Joe", "Walnes");
        joe.setPhone(new PhoneNumber(123, "1234-567"));
        joe.setFax(new PhoneNumber(123, "9999-999"));

        String xml = xstream.toXML(joe);
        System.out.print(xml);
        return xml;
    }

    public Person test2(String xml) {
        XStream xstream = new XStream();

        xstream.alias("person", Person.class);
        xstream.alias("phone", PhoneNumber.class);


        Person newJoe = (Person)xstream.fromXML(xml);
        return newJoe;
    }
}
