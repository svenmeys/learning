package be.dasos.learning.xstream.hashmaps;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.collections.MapConverter;
import com.thoughtworks.xstream.mapper.Mapper;

import java.util.HashMap;

public class Main {

    public static void main (String[] args) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value3");

        XStream xstream = new XStream();

        String xml = xstream.toXML(map);
        System.out.println(xml);

      //  MapConverter con = new MapConverter();

    }

}

class HashMapConverter extends MapConverter {

    public HashMapConverter(Mapper mapper) {
        super(mapper);
    }
}
