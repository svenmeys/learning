package be.dasos.learning.xstream.hashmaps;


import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.Map;

public class MapConvertor implements Converter {

    @Override
    public void marshal(Object o, HierarchicalStreamWriter writer, MarshallingContext context) {
        Map<String,String> map = (Map<String, String>)o;
        for(String key: map.keySet()){
        writer.startNode("property");
            //<propertyType id="cmis:name" propertyType="STRING" versioned="true" />
        }
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader hierarchicalStreamReader, UnmarshallingContext unmarshallingContext) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean canConvert(Class aClass) {
        return aClass == Map.class;
    }
}
