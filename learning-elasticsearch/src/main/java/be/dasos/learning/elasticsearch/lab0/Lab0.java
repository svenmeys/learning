package be.dasos.learning.elasticsearch.lab0;

import org.elasticsearch.client.AdminClient;
import org.elasticsearch.client.Client;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;

/**
 * This lab shows how to create and start an elasticsearch node.
 */
public class Lab0 {
    protected Node node;
    protected Client client;
    protected AdminClient admClient;

    public static void main(String args[]) throws Exception{
        Lab0 lab0 = new Lab0();

        lab0.startNode();

        // --Stop the program when input is received
        System.in.read();
    }

    protected void startNode() {
        // --Create a node. It starts a ES instance.
        node = NodeBuilder.nodeBuilder().node();

        // --The ES client.
        client = node.client();

        // --For admin privileged operations
        admClient = client.admin();
    }
}
