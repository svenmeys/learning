package be.dasos.learning.elasticsearch.lab1;

import be.dasos.learning.elasticsearch.beans.Beer;
import be.dasos.learning.elasticsearch.beans.Colour;
import be.dasos.learning.elasticsearch.lab0.Lab0;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.common.io.stream.OutputStreamStreamOutput;
import org.elasticsearch.common.io.stream.StreamOutput;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.io.ObjectOutputStream;

/**
 * This lab shows how to create json objects to use in ES.
 */
public class Lab1 extends Lab0 {
    public static void main(String[] args) throws Exception{
        Lab1 lab1 = new Lab1();
        lab1.startNode();

        // -- Do it manually
        System.out.println("Manual --> " + lab1.manualJson());

        // -- Use the content builder
        System.out.println("XCB --> " + lab1.xcbJson().string());

        // -- Create a beer object and serialize it using jackson.
        Beer beer = new Beer("Heineken", Colour.PALE, 0.33, 3);
        System.out.println("ObjMapper --> " + lab1.objectJson(beer));

        // --Lets index something (http://esurl:esport/index/type/id)
        String json =  lab1.objectJson(beer);

        IndexResponse response;
        response = lab1.client.prepareIndex("drinks","beer","1")
                       .setSource(json)                         // --Using a JSON String
                       .execute()
                       .actionGet();

        response = lab1.client.prepareIndex("index","type","id")
                       .setSource(lab1.xcbJson())               // --Can use xcb directly
                       .execute()
                       .actionGet();

        /* --Bulk insert
        BulkRequestBuilder brb = lab1.client.prepareBulk();
        brb.add(new IndexRequest("index","type").source(json));
        BulkResponse bulkResponse = brb.execute().actionGet();
        bulkResponse.writeTo(new OutputStreamStreamOutput(System.err));
        */

        // --Get something out
        GetResponse getResponse = lab1.client.prepareGet("drinks","beer","1").execute().actionGet();

        // --Deserialize the object
        ObjectMapper mapper = new ObjectMapper();
        Beer beer1 = mapper.readValue(getResponse.getSourceAsBytes(), Beer.class);
        System.out.println(beer1.toString());

        // --Delete objects
        lab1.client.prepareDelete("drinks","beer","1").execute().actionGet();

        /* --Delete in bulk
        BulkRequestBuilder brb = lab1.client.prepareBulk();
        brb.add(new DeleteRequest("index","type","id"));
        BulkResponse bulkResponse = brb.execute().actionGet();
        bulkResponse.writeTo(new OutputStreamStreamOutput(System.err));
        */

    }

    protected String manualJson() {
        return "{\"field\":\"value\"}";
    }

    protected XContentBuilder xcbJson () throws Exception {
        XContentBuilder xcb = XContentFactory.jsonBuilder()
                .startObject().field("field", "value")
                .endObject();
        return xcb;
    }

    protected String objectJson(Object o) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(o);
    }
}
