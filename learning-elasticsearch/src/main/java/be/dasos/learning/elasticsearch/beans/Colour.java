package be.dasos.learning.elasticsearch.beans;

public enum Colour {
    DARK,
    PALE,
    WHITE;
}