package be.dasos.learning.elasticsearch.lab2;

import be.dasos.learning.elasticsearch.beans.Beer;
import be.dasos.learning.elasticsearch.beans.Colour;
import be.dasos.learning.elasticsearch.lab1.Lab1;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.io.stream.OutputStreamStreamOutput;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

/**
 * This lab is all about searching and building queries
 */
public class Lab2 extends Lab1 {

    public static void main(String[] args) throws Exception {
        Lab2 lab = new Lab2();
        lab.startNode();
        Thread.sleep(1000);
        // --Let's build some queries
        QueryBuilder qMatchAll  = QueryBuilders.matchAllQuery();

        QueryBuilder qTerm      = QueryBuilders.termQuery("term","value");

        QueryBuilder qMatch     = QueryBuilders.matchQuery("fieldName","text");

        QueryBuilder qString    = QueryBuilders.queryString("select * from drinks");

        QueryBuilder qRange     = QueryBuilders.rangeQuery("fieldName").from("a").to("z");

        QueryBuilder qBool      = QueryBuilders.boolQuery()
                                    .must(QueryBuilders.termQuery("fieldName", "value"))
                                    .mustNot(QueryBuilders.termQuery("fieldName", "value2"))
                                    .should(QueryBuilders.rangeQuery("fieldName").from(1).to(5));
        // --Add stuff
        lab.addBeer();
        Thread.sleep(1000); // TODO find out a better way to do this

        // --Launch a search
        SearchResponse response = lab.client.prepareSearch().setQuery(qMatchAll).execute().actionGet();
        long executionTime = response.getTookInMillis();
        long totalHits = response.getHits().getTotalHits();

        // fixme find out why number of hits differs every time
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            // --Each hit contains some parameters
            System.out.printf("[hit] index: %s, type: %s, id: %s, score: %f version: %d|>>",
                               hit.index(), hit.type(), hit.id(), hit.score(), hit.version());
            // fixme find out if version = -1 is correct
            // --Can convert objects back to json
            System.out.println(hit.getSourceAsString());
        }
    }

    public void addBeer() throws Exception {
        // -- Create a beer object and serialize it using jackson.
        Beer beer = new Beer("Heineken", Colour.PALE, 0.33, 3);
        //System.out.println("ObjMapper --> " + objectJson(beer));

        // --Lets index something (http://esurl:esport/index/type/id)
        String json =  objectJson(beer);

        IndexResponse response;
        response = client.prepareIndex("drinks","beer")
                .setSource(json)                         // --Using a JSON String
                .execute()
                .actionGet();
    }
}
