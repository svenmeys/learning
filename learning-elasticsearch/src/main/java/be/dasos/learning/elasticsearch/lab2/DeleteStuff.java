package be.dasos.learning.elasticsearch.lab2;

import be.dasos.learning.elasticsearch.lab0.Lab0;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.common.io.stream.OutputStreamStreamOutput;

public class DeleteStuff extends Lab0 {

    public static void main(String[] args) throws Exception {
        DeleteStuff program = new DeleteStuff();
        program.startNode();

        BulkRequestBuilder brb = program.client.prepareBulk();
        brb.add(new DeleteRequest("index","type","ozRvHpgiSW2a8Yk74EP16A"));
        brb.add(new DeleteRequest("index","type","arUiCQ1KRH6cAPoHG1cSpw"));
        brb.add(new DeleteRequest("index","type","rx_fkcJNTaaumQFFvda43A"));
        brb.add(new DeleteRequest("index","type","Z11hTH8MRuaQhAa44mRDSA"));
        brb.add(new DeleteRequest("index","type","ZiNrzQ17RVy6M02kXS9ZOw"));
        brb.add(new DeleteRequest("index","type","id"));
        BulkResponse bulkResponse = brb.execute().actionGet();
        bulkResponse.writeTo(new OutputStreamStreamOutput(System.err));
        if (bulkResponse.hasFailures()) {
            System.out.println("Bulk request has failures " + bulkResponse.buildFailureMessage());
        }
        Thread.sleep(10000);
    }
}
