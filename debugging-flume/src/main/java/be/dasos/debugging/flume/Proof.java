package be.dasos.debugging.flume;

import com.google.common.base.Stopwatch;
import org.apache.flume.serialization.*;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.*;

public class Proof {
    Logger logger = Logger.getLogger(Proof.class);

    public static void main(String... args) throws Exception {
        File file = new File("/projects/data/telenet/files/hfc_cpe_iaa.csv");
        Proof proof = new Proof(file, DEFAULT_BUF_SIZE, Charset.forName(CHARSET_DFLT));
        BufferedWriter writer = new BufferedWriter(new FileWriter("/projects/data/telenet/files/hfc_cpe_iaa2.csv"));
        String line = null;
        int counter = 0;
        Stopwatch w = new Stopwatch();
        w.start();
        try {
        while ((line = proof.readLine()) != null) {
            ++counter;
            if(line.length() < 60) {
                System.out.println(line);
            }
            writer.write(line);
            writer.newLine();
        }
           writer.close();
        w.stop();
        System.out.print(w.elapsedMillis());

    } catch (Exception e) {
        System.out.println(counter + e.getMessage());
    }
    }

    /**
     * LineDeserializer methods
     *
     */

    public static final String CHARSET_DFLT = "UTF-8";
    public static final int MAXLINE_DFLT = 2048;
    private final int maxLineLength = MAXLINE_DFLT;


    // TODO: consider not returning a final character that is a high surrogate
    // when truncating
    private String readLine()  {
        StringBuilder sb = new StringBuilder();
        int c = 0;
        int readChars = 0;
        int counter = 0;
        while (counter < 16385) {
            try {
            c = readChar();
            } catch (Exception e) {
               // logger.debug(sb.toString());
                e.printStackTrace();
            }
            if (c==-1) {counter++; continue;}// logger.warn("EOF returned " + sb.toString()); continue;}
            readChars++;

            // FIXME: support \r\n
            if (c == '\n') {
                break;
            }

            sb.append((char)c);

            if (readChars >= maxLineLength) {
                logger.warn(String.format("Line length exceeds max (%d), truncating line!",
                        maxLineLength));
                break;
            }
        }

        if (readChars > 0) {
            return sb.toString();
        } else {
            return null;
        }
    }



 //-----------------------------------------
    /**
     * ResettableFileInputStream methods we use
     *
     */

    public static final int DEFAULT_BUF_SIZE = 16384;
    public static final int DEFAULT_BUF_MIN_SIZE = 8;
    private final File file;
    private final FileInputStream in;
    private final FileChannel chan;
    private final ByteBuffer buf;
    private final CharBuffer charBuf;
    private final byte[] byteBuf;
    private final long fileSize;
    private final CharsetDecoder decoder;
    private long position;
    private long syncPosition;
    private final int maxCharWidth;
    public Proof(File file, int bufSize, Charset charset)
            throws IOException {


        this.file = file;

        this.in = new FileInputStream(file);
        this.chan = in.getChannel();
        this.buf = ByteBuffer.allocateDirect(bufSize);
        buf.flip();
        this.byteBuf = new byte[1]; // single byte
        this.charBuf = CharBuffer.allocate(1); // single char
        charBuf.flip();
        this.fileSize = file.length();
        this.decoder = charset.newDecoder().onMalformedInput(CodingErrorAction.REPORT)
                                           .onUnmappableCharacter(CodingErrorAction.REPORT);

        maxCharWidth = (int)charset.newEncoder().maxBytesPerChar();
        logger.debug(maxCharWidth);
        this.position = 0;
        this.syncPosition = 0;
    }


    public synchronized int readChar() throws IOException {
        if (buf.remaining() < maxCharWidth) {
            buf.clear();
            buf.flip();
            refillBuf();
        }

        int start = buf.position();
        charBuf.clear();

        boolean isEndOfInput = false;
        if (position >= fileSize) {
            isEndOfInput = true;
        }

        CoderResult res = decoder.decode(buf, charBuf, isEndOfInput);
        int delta = buf.position() - start;

        if (res.isMalformed() || res.isUnmappable()) {
            res.throwException();
        }

        charBuf.flip();
        if (charBuf.hasRemaining()) {
            char c = charBuf.get();
            // don't increment the persisted location if we are in between a
            // surrogate pair, otherwise we may never recover if we seek() to this
            // location!
            incrPosition(delta, !Character.isHighSurrogate(c));
            return c;

            // there may be a partial character in the decoder buffer
        } else {
            incrPosition(delta, false);
            return -1;
        }
    }

    private void refillBuf() throws IOException {
        buf.compact();
        chan.position(position); // ensure we read from the proper offset   //fixme problem solved here [part2]. Warning: may blow up.
        chan.read(buf);
        buf.flip();
    }

    public synchronized void seek(long newPos) throws IOException {

        // check to see if we can seek within our existing buffer
        long relativeChange = newPos - position;
        if (relativeChange == 0) return; // seek to current pos => no-op

        long newBufPos = buf.position() + relativeChange;
        if (newBufPos >= 0 && newBufPos < buf.limit()) {
            // we can reuse the read buffer
            buf.position((int)newBufPos);
        } else {
            // otherwise, we have to invalidate the read buffer
            buf.clear();
            buf.flip();
        }

        // clear decoder state
        decoder.reset();

        // perform underlying file seek
        chan.position(newPos);

        // reset position pointers
        position = syncPosition = newPos;
    }

    private void incrPosition(int incr, boolean updateSyncPosition) {
        position += incr;
        if (updateSyncPosition) {
            syncPosition = position;
        }
    }

    public void close() throws IOException {
        in.close();
    }
}
